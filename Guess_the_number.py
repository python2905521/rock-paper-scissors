print("Let's play 'Guess the number'!\nI will think of a number between 1 and 100 and you have to guess it correctly.\nYou will have a total of eight chances to guess the correct number.\n")

import random
number = (random.randint(1,100))
counter = 1
attempts = 8
while True:
    if attempts == 0:
        print("You have run out of chances. Better luck next time.")
        exit()
    guess = int(input("Enter your guess: "))
    highLow = number - guess
    if guess <= 0 or guess > 100:
        print("Invalid number detected. Re-run the program. Enter a number between 1 and 100.")
        exit()   
    elif guess < number and highLow <= 10:
        counter += 1
        attempts -= 1 
        print(f"Low, but close. You have {attempts} chance(s) left to guess the correct number.")
    elif guess > number and highLow >= -10:
        counter += 1
        attempts -= 1
        print(f"High, but close. You have {attempts} chance(s) left to guess the correct number.")
    elif guess < number:
        counter += 1
        attempts -= 1
        print(f"Too low. You have {attempts} chance(s) left to guess the correct number.")
    elif guess > number:
        counter += 1
        attempts -= 1
        print(f"Too high. You have {attempts} chance(s) left to guess the correct number.") 
    else: 
        break

print(f"Congrats, you guessed the number correctly. It took you {counter} guesse(s) to get the correct number.")             