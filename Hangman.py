import random

listOfWords = ["art", "hangman", "car", "evil", "genius", "malayalam", "kerala", "cat", "dog", "milk"]

letterPicked = []

lives = 7

word = random.choice(listOfWords)

while True:
  letter = input("\n\nEnter a single letter: ").lower()
  if len(letter) == 1 and letter.isalpha():
    if letter in letterPicked:
      print("You have tried that already")
      continue 
    letterPicked.append(letter) 
    if letter in word:
      print("Correct!")
      allLetters = True
      for i in word:
        if i in letterPicked:
          print(i,end='')
        else:
          print("_", end='')
          allLetters = False
      if allLetters == True:
          print(f"\nYou won with {lives} lives left.")
          exit()    
    
    
    else:
      print("Nope, not in there.")
      lives -= 1
      if lives == 6:
        print(f"{lives} chances left")
        print('''
  +---+
  |   |
      |
      |
      |
      |
=========''')  
      elif lives == 5:
       print(f"{lives} chances left")
       print('''
  +---+
  |   |
  O   |
      |
      |
      |
=========''')
      elif lives == 4:
        print(f"{lives} chances left")
        print('''
  +---+
  |   |
  O   |
  |   |
      |
      |
=========''')
      elif lives == 3:
        print(f"{lives} chances left")
        print('''
  +---+
  |   |
  O   |
 /|   |
      |
      |
=========''')
      elif lives == 2:
        print(f"{lives} chances left")
        print( '''
  +---+
  |   |
  O   |
 /|\  |
      |
      |
=========''')
      elif lives == 1:
        print(f"{lives} chance left")
        print('''
  +---+
  |   |
  O   |
 /|\  |
 /    |
      |
=========''')
      elif lives == 0:
        print(f"{lives} chances left. You lost. Better luck next time")
        print('''
  +---+
  |   |
  O   |
 /|\  |
 / \  |
      |
=========''')
        exit()

  
  else:
    print("Invalid input. Please enter a single letter.")    