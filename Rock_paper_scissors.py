from getpass import getpass as input
print("Let's play Rock Paper scissors!\nTwo players can play this game.\nThere will be three rounds of 5 points.\nWhoever wins two rounds first, wins the game.\nTo play 'Rock' press 'r', to play 'Paper' press 'p', to play 'Scissors' press 's'.\n")
name1 = input("Enter name of first player: ")
name2 = input("Enter name of second player: ")
score1 = 0
score2 = 0
counter1 = 0
counter2 = 0
counter = 1
while True:
   if counter == 1: 
    print("Starting round 1...") 
   while score1 < 5 and score2 < 5:     
      player1 = input(f"{name1}>") 
      player2 = input(f"{name2}>")  
      if player1 == "r" and player2 == 'p':
        score2 += 1
        print(f"Paper wins. Now {name2} has {score2} point(s).")
      elif player1 == "p" and player2 == 'r':
        score1 += 1
        print(f"Paper wins. Now {name1} has {score1} point(s).")
      elif player1 == 'r' and player2 == 's':
        score1 += 1
        print(f"Rock wins. Now {name1} has {score1} point(s).")
      elif player1 == "s" and player2 == 'r':
        score2 += 1
        print(f"Rock wins. Now {name2} has {score2} point(s).")
      elif player1 == "p" and player2 == 's':
        score2 += 1
        print(f"Scissors wins. Now {name2} has {score2} point(s).")
      elif player1 == "s" and player2 == 'p':
        score1 += 1
        print(f"Scissors wins. Now {name1} has {score1} point(s).")
      elif player1 == "s" and player2 == "s" or player1 == "r" and player2 == "r" or player1 == "p" and player2 == "p":
        print("It's a draw. Play again.")
      else:
        print("Invalid character detected. Try again. Press 's', 'p' or 'r'.")   
   if score1 > score2:
        print(f"{name1} scores {score1} points. {name1} wins round {counter}.")
        counter1 += 1
   else:
        print(f"{name2} scores {score2} points. {name2} wins round {counter}.")
        counter2 += 1        
   score1 -= score1 
   score2 -= score2   
   print(f"Round {counter} completed.")
   counter += 1 
   if counter1 == 2:
    print(f"{name1} won 2 rounds. Therefore {name1} wins the game.🥳✌️")
    exit()
   elif counter2 == 2:
    print(f"{name2} won 2 rounds. Therefore {name2} wins the game.🥳✌️") 
    exit()   
   else:
    print(f"Starting round {counter}...")
    continue      